package models;

import campaign.models.Drone;

public class Act {

    public static final int ADVANCE         = 0x00;
    public static final int RETREAT         = 0x01;
    public static final int TURN_LEFT       = 0x02;
    public static final int TURN_RIGHT      = 0x03;
    public static final int LOCK_TARGET     = 0x04;
    public static final int UNLOCK_TARGET   = 0x05;

    public Drone actor;
    public Drone target;
    public int action;
    public int progressTimer;

    public Act(int progressTimer, int action, Drone actor) {
        this(progressTimer, action, actor, actor);
    }

    public Act(int progressTimer, int action, Drone actor, Drone target) {
        this.progressTimer = progressTimer;
        this.action = action;
        this.actor = actor;
        this.target = target;
    }

    public boolean isFinished() {
        if (progressTimer <= 0)
            return true;
        progressTimer--;
        return false;
    }

}
