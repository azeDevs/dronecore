package models;

public class Axial {


    public static final Axial UP_RIGHT   = new Axial( 1, -1);
    public static final Axial RIGHT      = new Axial( 1,  0);
    public static final Axial DN_RIGHT   = new Axial( 0,  1);
    public static final Axial DN_LEFT    = new Axial( 1, -1);
    public static final Axial LEFT       = new Axial(-1,  0);
    public static final Axial UP_LEFT    = new Axial( 0, -1);

    public int col;
    public int row;


    public Axial() { this(0, 0); }
    public Axial(Axial h) { this(h.col, h.row); }
    public Axial(Cube c) { this(c.toAxial()); }
    public Axial(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public Axial getCoord() {
        return new Axial(col, row);
    }

    public static Axial getAdjacent(int i) {
        switch(i) {
            case 0: return UP_RIGHT;
            case 1: return RIGHT;
            case 2: return DN_RIGHT;
            case 3: return DN_LEFT;
            case 4: return LEFT;
            case 5: return UP_LEFT;
        }
        return null;
    }

    public boolean isSameCoordsAs(Axial axial) {
        if (col == axial.col && row == axial.row)
            return true;
        else
            return false;
    }

    public void setSameCoordAs(Axial axial) {
        col = axial.col;
        row = axial.row;
    }

    public void setCoord(Axial h) {
        setCoord(h.col, h.row);
    }

    public void setCoord(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public void setCoord(double col, double row) {
        this.col = (int) Math.floor(col+0.5);
        this.row = (int) Math.floor(row+0.5);
    }

    public void translate(Axial h) {
        translate(h.getCol(), h.getRow());
    }

    public void translate(int dCol, int dRow) {
        this.col += dCol;
        this.row += dRow;
    }

    public Cube toCube() {
        int col = this.col;
        int z   = this.row;
        int row = -col - z;
        return new Cube(col, row, z);
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getStringWithLegend() {
        return "C" + col + ",R" + row;
    }

    public String getString() {
        return col + "," + row;
    }

}
