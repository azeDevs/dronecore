package models;

import campaign.models.Locale;

public class Grid {

    public Locale[][] hex;
    public final int cols;
    public final int rows;


    public Grid(int size) {
        this.cols = size;
        this.rows = size;
        this.hex = new Locale[cols][rows];
    }

    /*
        TODO: convert incoming Axial from middle being zero.
        add midRow to value.
        check for out of bounds.
    */


    public Locale getHex(int col, int row) {
        return hex[col][row];
    }
    public Locale getHex(Axial axial) {
        return hex[axial.col][axial.row];
    }
}
