package models;

public class Cube {


    public static final Cube UP_RIGHT   = new Cube(1, 0, -1);
    public static final Cube RIGHT      = new Cube(1, -1, 0);
    public static final Cube DN_RIGHT   = new Cube(0, -1, 1);
    public static final Cube DN_LEFT    = new Cube(-1, 0, 1);
    public static final Cube LEFT       = new Cube(-1, 1, 0);
    public static final Cube UP_LEFT    = new Cube(0, 1, -1);

    public int x;
    public int y;
    public int z;


    public Cube() { this(0, 0, 0); }
    public Cube(Cube c) { this(c.x, c.y, c.z); }
    public Cube(Axial h) { this(h.toCube()); }
    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Cube getCoord() {
        return new Cube(x, y, z);
    }

    public static Cube getAdjacent(int i) {
        switch(i) {
            case 0: return UP_RIGHT;
            case 1: return RIGHT;
            case 2: return DN_RIGHT;
            case 3: return DN_LEFT;
            case 4: return LEFT;
            case 5: return UP_LEFT;
        }
        return null;
    }

    public boolean isSameCoordsAs(Cube coord) {
        if (x == coord.x && y == coord.y && z == coord.z)
            return true;
        else
            return false;
    }

    public void setSameCoordAs(Cube coord) {
        x = coord.x;
        y = coord.y;
        z = coord.z;
    }

    public void setCoord(Cube c) {
        setCoord(c.x, c.y, c.z);
    }

    public void setCoord(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setCoord(double x, double y, double z) {
        this.x = (int) Math.floor(x+0.5);
        this.y = (int) Math.floor(y+0.5);
        this.z = (int) Math.floor(y+0.5);
    }

    public void translate(Cube c) {
        translate(c.getX(), c.getY(), c.getZ());
    }

    public void translate(int dx, int dy, int dz) {
        this.x += dx;
        this.y += dy;
        this.z += dz;
    }

    public Axial toAxial() {
        int col = this.x;
        int row = this.z;
        return new Axial(col, row);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public String getAsString() {
        return "X" + x + ",Y" + y + ",Z" + z;
    }

}
