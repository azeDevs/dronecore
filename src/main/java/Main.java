import campaign.Campaign;

public class Main {

    public static void main(String[] args) {
        Campaign droneCore = new Campaign();
        droneCore.start();
    }

}
