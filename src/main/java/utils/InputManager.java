package utils;

import java.util.Scanner;

public class InputManager {


    private Scanner scanner;


    public InputManager() {
        scanner = new Scanner(System.in);
    }

    protected int getCommand(String... commands) {
        int choice = -1;
        while (choice == -1) {
            choice = parseCommand(commands);
            if (choice == -1) System.out.print("\nAct not recognized, please try again.\n");
        }
        return choice;
    }

    protected String getInput() {
        System.out.print("▼\n");
        String input = scanner.next();
        scanner.nextLine();
        return input;
    }

    protected String getSaveGameName() {
        OutputManager.printSaves();
        String input = scanner.next();
        scanner.nextLine();
        return input;
    }

    protected void getAnyKey() {
        System.out.print("↵");
        scanner.nextLine();
    }


    private int parseCommand(String[] commands) {
        for (String choice : commands)
            System.out.print("▪ " + choice + " ");

        System.out.print("\n");
        String input = scanner.next();
        scanner.nextLine();

        for (int i = 0; i < commands.length; i++)
            if (input.equals(commands[i]))
                return i;

        return -1;
    }

}
