package utils;

import campaign.models.Character;
import campaign.models.Drone;
import models.Act;

import java.util.ArrayList;

public class OutputManager extends InputManager {

    public static final boolean PRINT_PAUSE = false;
    public static final boolean READ_COMBAT = true;
    public static final int PAUSE = 20;
    private Character player;

    /*
     *  STORY TELLING
     */
    protected void header(String header) {
        out("\n\n━━[ " + header + " ]");
        int replacedCharacter = 64 - header.length();
        for (int i = 0; i < replacedCharacter; i++)
            out("━");
    }

    protected void print(String path, String page) {
        String text = FileUtil.getTextResource(path);
        printParagraphs(text, page);
    }

    protected static void printSaves() {
        ArrayList<String> listOfSaves = FileUtil.getSavesList();
        for (String save : listOfSaves) {
            out("◆ " + save + " ");
        }
        System.out.println();
    }

    protected void setPlayer(Character player) {
        this.player = player;
    }


    protected void printParagraphs(String string, String page) {
        int charCount = 0;
        String code = "";
        boolean startCode = false;
        boolean parseCode = false;
        boolean printPage = false;

        // step through text
        for (int i = 0; i < string.length(); i++) {

            // parse code
            if (String.valueOf(string.charAt(i)).equals("{") || parseCode) {
                parseCode = true;
                code += String.valueOf(string.charAt(i));

                if (String.valueOf(string.charAt(i)).equals("}"))
                    parseCode = false;
            }

            // execute code
            if (!parseCode && !code.equals("")) {
                if (code.equals("{#" + page + "}")) {
                    printPage = true;
                    startCode = true;
                }
                if (startCode && code.equals("{designation}")) {
                    out(player.getName());
                }
                if (startCode && code.equals("{location}")) {
                    out(player.getLocation().getStringWithLegend());
                }
                if (startCode && code.equals("{loading}")) {
                    animateProgressBar();
                }
                if (startCode && code.equals("{anykey}")) {
                    super.getAnyKey();
                }
                if (startCode && code.equals("{/#}")) {
                    break;
                }
                code = "";
                i++;
            }

            // print text
            if (!parseCode && printPage) {
                out(String.valueOf(string.charAt(i)));
                if (PRINT_PAUSE) charCount++;
                if (charCount >= 140) { pause(PAUSE); charCount = 0; }
            }
        }
    }

    protected void animateProgressBar() {
        for (int i = 0; i < 16; i++) {
            printProgressBar();
        }
        outln();
    }

    /*
     *  COMBAT
     */
    protected void getDroneInspection(Drone drone) {
        out("\n╸[ " + drone.designation + " ]");
        out("\n╸  ┖┈┈┈[ shield   " + drone.getShield() + " / 9");
        out("\n╸      [ offense  " + drone.getOffense());
        out("\n╸      [ defense  " + drone.getDefense());
        out("\n╸      [ location " + drone.getLocation().getStringWithLegend());
        out("\n╸      [ target   " + (drone.getTarget() == null ? "n/a" : drone.getTarget().designation));
        pause(PAUSE);
    }

    protected void printPreCycleInfo(ArrayList<Act> actQueue) {
        if (!finishedActs(actQueue)) {
            out("\n");
            for (Act act : actQueue) {
                reportProgress(act);
                pause(PAUSE);
            }
            if (READ_COMBAT) {
                out("\n- Time is passing... Δ ");
            } else {
                out("\n-  Processing Δ ");
            }
            pause(PAUSE);
        }
    }

    protected void reportProgress(Act act) {
        if (READ_COMBAT) {
            if (act.actor == act.target) out("\n╸ " + act.actor.designation + " is " + getActString(act) + "                      ");
            else out("\n╸ " + act.actor.designation + " is " + getActString(act) + " on " + act.target.designation + "    ");
            out("ETA: " + act.progressTimer);
        } else {
            if (act.actor == act.target) out("\n╸[ " + act.actor.designation + " power " + getActString(act) + "() ]    ");
            else out("\n╸[ " + act.actor.designation + " " + getActString(act) + "( " + act.target.designation + " ) ]    ");
            out("ETA: " + act.progressTimer);
        }
        pause(PAUSE);
    }

    protected void finishAction(Act act) {
        if (READ_COMBAT) {
            if (act.actor == act.target) out("\n─ " + act.actor.designation + " " + getActString(act) + "                         ");
            else out("\n─ " + act.actor.designation + " " + getActString(act) + " on " + act.target.designation + "       ");
            pause(PAUSE);
            out("COMPLETE");
        } else {
            if (act.actor == act.target) out("\n─[ " + act.actor.designation + " power " + getActString(act) + "() ]    ");
            else out("\n─[ " + act.actor.designation + " " + getActString(act) + "( " + act.target.designation + " ) ]    ");
            pause(PAUSE);
            out("COMPLETE");
        }
        pause(PAUSE);
    }

    protected void printProgressBar() {
        out("█▉");
        pause(PAUSE);
    }

    protected void printBattleResults() {
        pause(PAUSE);
        if (READ_COMBAT) {
            out("\n\n-[ Combat is concluded Δ");
        } else {
            out("\n\n-[ Concluding Δ");
        }
        pause(PAUSE * 6);
    }


    private boolean finishedActs(ArrayList<Act> actQueue) {
        for (Act act : actQueue)
            if (act.progressTimer <= 0) return true;
        return false;
    }

    private String getActString(Act act) {
        if (READ_COMBAT) {
            switch (act.action) {
                case Act.ADVANCE:
                    return "ADVANCING FORWARD";
                case Act.RETREAT:
                    return "RETREATING BACKWARD";
                case Act.TURN_LEFT:
                    return "PIVOTING LEFT";
                case Act.TURN_RIGHT:
                    return "PIVOTING RIGHT";
                case Act.LOCK_TARGET:
                    return "LOCKING TARGET";
                case Act.UNLOCK_TARGET:
                    return "UNLOCKING TARGET";
            }
        } else {
            switch (act.action) {
                case Act.ADVANCE:
                    return "engine_forward";
                case Act.RETREAT:
                    return "engine_reverse";
                case Act.TURN_LEFT:
                    return "engine_trnLeft";
                case Act.TURN_RIGHT:
                    return "engine_trnRght";
                case Act.LOCK_TARGET:
                    return "target";
                case Act.UNLOCK_TARGET:
                    return "safety";
            }
        }
        return null;
    }

    private void pause(int time) {
        try { Thread.sleep(time * 11); } catch (InterruptedException e) { e.printStackTrace(); }
    }


    /*
     *  UTILITY
     */
    private static void out(String output) {
        for (int i = 0; i < output.length(); i++) {
            String c = String.valueOf(output.charAt(i));
            System.out.print(c);
        }
    }

    private void outln(String output) {
        out("\n" + output);
    }

    private void outln() {
        out("\n");
    }

}
