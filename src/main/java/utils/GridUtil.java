package utils;

import campaign.constants.Terrain;
import campaign.models.Locale;
import models.Axial;
import models.Grid;

public final class GridUtil {

    public static void debugPopulation(Grid grid) {
        addRandomTerrain(grid);
        printCropValues(grid);
        cropLocales(grid);
        printGrid(grid, false, true);
        printGrid(grid, true, false);
        grid.getHex(2,0).setTerrain(Terrain.NULL);
        grid.getHex(0,2).setTerrain(Terrain.NULL);
        grid.getHex(1,4).setTerrain(Terrain.NULL);
        grid.getHex(4,1).setTerrain(Terrain.NULL);
        grid.getHex(4,4).setTerrain(Terrain.NULL);
        printGrid(grid, false, true);
        printGrid(grid, true, false);
    }

    public static void addTerrain(Grid grid) {
        for (int col = 0; col < grid.cols; col++)
            for (int row = 0; row < grid.rows; row++)
                grid.hex[col][row] = new Locale(Terrain.FLAT);
    }

    public static void addRandomTerrain(Grid grid) {
        for (int col = 0; col < grid.cols; col++) {
            for (int row = 0; row < grid.rows; row++) {
                int def = (int) Math.round(Math.random() * 0);

                switch (def) {
                    case 0:
                        grid.hex[col][row] = new Locale(Terrain.FLAT);
                        break;
                    case 1:
                        grid.hex[col][row] = new Locale(Terrain.HILL);
                        break;
                    case 2:
                        grid.hex[col][row] = new Locale(Terrain.MOUNTAIN);
                        break;
                    case 3:
                        grid.hex[col][row] = new Locale(Terrain.BRIDGE_H);
                        break;
                    case 4:
                        grid.hex[col][row] = new Locale(Terrain.BRIDGE_R);
                        break;
                    case 5:
                        grid.hex[col][row] = new Locale(Terrain.BRIDGE_L);
                        break;
                    case 6:
                        grid.hex[col][row] = new Locale(Terrain.PIT);
                        break;
                    default:
                        grid.hex[col][row] = new Locale(Terrain.NULL);
                        break;
                }

            }
        }
    }

    public static void printGrid(Grid grid, boolean alignAsHex, boolean asAxial) {
        System.out.print("\n\n─ GridUtil [ size = " + grid.cols + " ]");
        if (alignAsHex)
            System.out.print(" OFFSET");
        else
            System.out.print(" NORMAL");
        System.out.print("\n─ ▁");
        for (int col = 0; col < grid.cols; col++)
            System.out.print("▁▁▁▁");
        for (int row = 0; row < grid.rows; row++) {
            System.out.print("\n─|\n─| ");
            if (alignAsHex)
                for (int i = 0; i < grid.rows-row; i++)
                    System.out.print("  ");
            else
                for (int i = 0; i < grid.rows; i++)
                    System.out.print(" ");

            for (int col = 0; col < grid.cols; col++) {
                int terrain = grid.hex[col][row].getTerrain();


                if (grid.hex[col][row].getDrone() != null)
                    System.out.print(" 웃  ");
                else if (terrain == Terrain.NULL && grid.hex[col][row].getDrone() != null)
                    System.out.print("!웃! ");
                else if (terrain != Terrain.NULL && asAxial)
                    System.out.print(new Axial(col, row).getString() + " ");
                else
                    switch (terrain) {
                        case Terrain.NULL:
                            System.out.print("    ");
                            break;
                        case Terrain.FLAT:
                            System.out.print(" ·  ");
                            break;
                        case Terrain.HILL:
                            System.out.print(" ◠  ");
                            break;
                        case Terrain.MOUNTAIN:
                            System.out.print(" ▲  ");
                            break;
                        case Terrain.BRIDGE_H:
                            System.out.print(" ─  ");
                            break;
                        case Terrain.BRIDGE_R:
                            System.out.print(" ╱  ");
                            break;
                        case Terrain.BRIDGE_L:
                            System.out.print(" ╲  ");
                            break;
                        case Terrain.PIT:
                            System.out.print(" x  ");
                            break;
                    }

            }
        }
        System.out.print("\n─|\n");
    }

    public static void printCropValues(Grid grid) {
        final int midRow = (grid.rows / 2);
        System.out.print("\n\nTOTAL_ROWS = ");
        alignedPrint(grid.rows);
        System.out.print("\nMIDDLE_ROW = ");
        alignedPrint(midRow);
        System.out.print("\nDIFFERENCE = ");
        for (int row=0; row < grid.rows; row++) alignedPrint(getDifference(row, midRow));
        System.out.print("\n START_COL = ");
        for (int row=0; row < grid.rows; row++) alignedPrint(getTargetStartCol(grid, row, midRow));
        System.out.print("\n FINAL_COL = ");
        for (int row=0; row < grid.rows; row++) alignedPrint(getTargetFinalCol(grid, row, midRow));
        System.out.print("\n");
    }


    public static void cropLocales(Grid grid) {
        final int midRow = (grid.rows / 2);
        for (int row = 0; row<grid.rows; row++) {
            for (int col = 0; col<grid.cols; col++) {
                int targetStartCol = getTargetStartCol(grid, row, midRow);
                int targetFinalCol = getTargetFinalCol(grid, row, midRow);
                for (int step = targetStartCol; step <= targetFinalCol; step++)
                    if (step >= 0)
                        grid.hex[step][row].setTerrain(Terrain.NULL);
            }
        }
    }

    private static int getTargetStartCol(Grid grid, int row, int midRow) {
        int targetStart = grid.rows + getDifference(row, midRow);
        if (targetStart == grid.rows)
            targetStart += -grid.rows - 1;
        if (targetStart > grid.rows)
            targetStart = 0;
        return targetStart;
    }

    private static int getTargetFinalCol(Grid grid, int row, int midRow) {
        int targetFinal = grid.rows - 1;
        if (grid.rows + getDifference(row, midRow) >= grid.rows )
            targetFinal = (-1 + getDifference(row, midRow));
        return targetFinal;
    }

    private static void alignedPrint(int target) {
        String before = "";
        String after = "  ";
        if (Math.signum(target) != -1)
            before = " ";
        if (target >= 10)
            after = " ";
            System.out.print(before + target + after);
    }

    private static int getDifference(int row, int midRow) {
        return row - midRow;
    }

}
