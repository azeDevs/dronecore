package utils;

import campaign.models.Character;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public final class FileUtil {


    public static final String HOME_DIR = System.getProperty("user.dir") + "/src/main";
    public static final String SAVES_DIR = HOME_DIR + "/saves/";

    private static final String NAME = "FIRST_NAME";
    private static final String CREDITS = "CREDITS";
    private static final String MAP_COL = "MAP_COL";
    private static final String MAP_ROW = "MAP_ROW";


    public static void saveGame(Character player) {
        try {
            String path = SAVES_DIR + player.getName();
            File file = new File(path);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

            writer.write(setValue(NAME, player.getName()));
            writer.write(setValue(CREDITS, player.getCredits()));
            writer.write(setValue(MAP_COL, player.getLocation().getCol()));
            writer.write(setValue(MAP_ROW, player.getLocation().getRow()));

            writer.close();

        } catch (IOException e) { e.printStackTrace(); }
    }

    public static void loadGame(Character player) {
        try {
            String path = SAVES_DIR + player.getName();
            FileInputStream fileInputStream = new FileInputStream(path);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(CREDITS)) { player.setCredits(getIntValue(line)); }
                if (line.contains(MAP_COL)) { player.getLocation().setCol(getIntValue(line)); }
                if (line.contains(MAP_ROW)) { player.getLocation().setRow(getIntValue(line)); }
            }

            reader.close();

        } catch (IOException e) { e.printStackTrace(); }
    }

    public static String getTextResource(String path) {
        try { return new String(Files.readAllBytes(Paths.get(FileUtil.HOME_DIR + "/resources" + path))); }
        catch (IOException e) { System.out.println("\nFATAL: nothing found at " + path); }
        return "null";
    }

    public static ArrayList<String> getSavesList() {
        File folder = new File(SAVES_DIR);
        File[] listOfFiles = folder.listFiles();
        ArrayList<String> listOfSaves = new ArrayList<>();

        for (int i = 0; i < listOfFiles.length; i++)
            if (listOfFiles[i].isFile())
                listOfSaves.add(listOfFiles[i].getName());

        return listOfSaves;
    }


    private static String setValue(String saveValue, String playerValue) {
        return saveValue + "=" + playerValue + "\n";
    }

    private static String setValue(String saveValue, int playerValue) {
        return saveValue + "=" + playerValue + "\n";
    }

    private static Integer getIntValue(String currentLine) {
        int loadedValue = Integer.parseInt(currentLine.substring(currentLine.lastIndexOf("=") + 1));
        return loadedValue;
    }
}
