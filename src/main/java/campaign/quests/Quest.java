package campaign.quests;

public abstract class Quest {

    /*

        Quests will be extended by Campaign-like classes
        containing all switches, interactions, and flow.

     */

    public abstract void start();

    public abstract void success();

    public abstract void failure();

}
