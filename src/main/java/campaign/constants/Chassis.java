package campaign.constants;

public class Chassis {

    public static final int SCOUT        = 0x00;
    public static final int MEDIUM       = 0x01;
    public static final int HEAVY        = 0x02;

    public static int getDefense(int type) {
        if (type == Chassis.SCOUT)
            return 1;
        if (type == Chassis.MEDIUM)
            return 2;
        if (type == Chassis.HEAVY)
            return 3;
        return -1;
    }

}
