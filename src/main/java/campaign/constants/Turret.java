package campaign.constants;

public final class Turret {

    public static final int SNIPER      = 0x00;
    public static final int ROCKET = 0x01;
    public static final int CANNON      = 0x02;

    public static int getOffense(int type) {
        if (type == Chassis.SCOUT)
            return 1;
        if (type == Chassis.MEDIUM)
            return 2;
        if (type == Chassis.HEAVY)
            return 3;
        return -1;
    }

}
