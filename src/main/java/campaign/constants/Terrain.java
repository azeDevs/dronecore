package campaign.constants;

public final class Terrain {

    public static final int NULL        = 0x000;
    public static final int FLAT        = 0x001;
    public static final int HILL        = 0x002;
    public static final int MOUNTAIN    = 0x003;
    public static final int BRIDGE_H    = 0x004;
    public static final int BRIDGE_R    = 0x005;
    public static final int BRIDGE_L    = 0x006;
    public static final int PIT         = 0x007;

}
