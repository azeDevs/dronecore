package campaign.constants;

public final class Structure {

    public static final int NULL        = 0x000;
    public static final int SETTLEMENT  = 0x001;
    public static final int CACHE       = 0x002;
    public static final int TURRET      = 0x003;
    public static final int DEBRIS      = 0x004;
    public static final int RESOURCE    = 0x005;

}
