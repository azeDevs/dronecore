package campaign;

import campaign.models.Battle;
import campaign.models.Character;
import models.Grid;
import utils.FileUtil;
import utils.GridUtil;
import utils.OutputManager;

public class Campaign extends OutputManager {


    private Character player;


    public Campaign() {
        this.player = new Character();
    }

    private void debug() {
        header("Debug Mock Battle");
        Grid grid = new Grid(5);
        GridUtil.debugPopulation(grid);
        getAnyKey();

        player.setName("Malen");
        Character taika = new Character().setName("Taika");

        Battle battle = new Battle(player, taika, 5);
        battle.startBattle();
    }

    /*
        The campaign begins here
    */
    public void start() {
        print("/common", "title");
        debug();
        header("Introduction");
        print("/intro", "introduction");
        switch (getCommand("login", "load", "exit")) {
            case 0:
                header("MDCN Login");
                introLogin();
                break;
            case 1:
                introLoad();
                break;
            case 2:
                exitGame();
                break;
        }
    }

    /*
        Load existing game
    */
    private void introLoad() {
        print("/intro", "loggingIn");
        player.setName(getSaveGameName());
        FileUtil.loadGame(player);
        navigateTerrain();
    }

    /*
        Create new game
    */
    private void introLogin() {
        print("/intro", "loggingIn");
        player.setName(getInput());
        print("/intro", "isThatYou");
        switch (getCommand("yes", "retry")) {
            case 0:
                FileUtil.saveGame(player);
                print("/intro", "loginComplete");
                header("Departing");
                print("/intro", "hitTheRoad");
                navigateTerrain();
                break;
            case 1:
                introLogin();
                break;
        }
    }

    /*
        General navigation
    */
    private void navigateTerrain() {
        print("/common", "grid2");
        print("/intro", "whereToGo");
        switch (getCommand("northeast", "east", "southeast", "southwest", "west", "northwest")) {
            case 1:
                exitGame();
                break;
            default:
                print("/common", "directionImpossible");
                navigateTerrain();
                break;
        }
    }

    /*
        Exit the game
    */
    private void exitGame() {
        print("/common", "exitingGame");
    }
}
