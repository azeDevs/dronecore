package campaign.models;

import campaign.constants.Chassis;
import campaign.constants.Turret;
import models.Axial;

public class Drone {

    public String designation;
    public int chassis;
    public int turret;

    private int shield;
    private int offense;
    private int defense;

    private Axial location;
    private Drone target;

    public Drone(int chassis, int turret) {
        this.chassis = chassis;
        this.turret = turret;
        init();
    }

    private void init() {
        shield = 9;
        offense = Turret.getOffense(chassis);
        defense = Chassis.getDefense(chassis);
        location = new Axial();
        designation = "";
    }


    public void setDesignation(Character character) {
        designation += character.getName().substring(0,3).toUpperCase() + "/";
        switch (chassis) {
            case Chassis.HEAVY: designation += "hvy_"; break;
            case Chassis.MEDIUM: designation += "med_"; break;
            case Chassis.SCOUT: designation += "sct_"; break;
        }
        switch (turret) {
            case Turret.SNIPER: designation += "Snpr"; break;
            case Turret.ROCKET: designation += "Rckt"; break;
            case Turret.CANNON: designation += "Cnnn"; break;
        }
    }

    public Drone getTarget() {
        return target;
    }

    public void setTarget(Drone target) {
        this.target = target;
    }

    public Axial getLocation() {
        return location;
    }

    public int getShield() {
        return shield;
    }

    public int getOffense() {
        return offense;
    }

    public int getDefense() {
        return defense;
    }
}
