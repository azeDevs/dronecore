package campaign.models;

import campaign.quests.Quest;
import models.Act;
import models.Axial;

import java.util.ArrayList;

public class Character {


    private String name;
    private String description;
    private int    credits;

    public Axial location;
    public ArrayList<Quest> quests;
    public ArrayList<Drone> drones;
    public ArrayList<Act> act;


    public Character() {
        this.location = new Axial();
        this.quests = new ArrayList<>();
        this.drones = new ArrayList<>();
    }

    public Character setName(String name) {
        this.name = name;
        return this;
    }
    public void setCredits(int credits) {
        this.credits = credits;
    }

    public String getName() {
        return name;
    }
    public Axial getLocation() {
        return location;
    }
    public Drone getDrone(int index) {
        if (index >= 0 && index < drones.size())
            return drones.get(index);
        return null;
    }
    public int getCredits() {
        return credits;
    }

    public void addCredits(int credits) {
        this.credits += credits;
    }
    public void addDrone(Drone drone) {
        drones.add(drone);
    }
}
