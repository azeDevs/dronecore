package campaign.models;

import campaign.quests.Quest;

import java.util.ArrayList;

public class Locale {

    private int terrain;
    private int structure;
    private String description;

    public ArrayList<Quest> quests;
    public ArrayList<Character> characters;
    public ArrayList<Battle> battles;
    public Drone drone;


    public Locale(int terrain) {
        this.terrain = terrain;
    }

    public int getTerrain() {
        return terrain;
    }

    public void setTerrain(int terrain) {
        this.terrain = terrain;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

}
