package campaign.models;

import campaign.constants.Chassis;
import campaign.constants.Turret;
import models.Act;
import models.Grid;
import utils.GridUtil;
import utils.OutputManager;

import java.util.ArrayList;
import java.util.Iterator;

public class Battle extends OutputManager{

    private Grid grid;

    private Character pl;
    private Character ai;
    private ArrayList<Act> actQueue;

    public Battle(Character pl, Character ai, int gridSize) {
        this.pl = pl;
        this.ai = ai;
        this.grid = new Grid(gridSize);
        this.actQueue = new ArrayList<>();
    }

    public void startBattle() {
        initBattle();
        playBattle();
        finishBattle();
    }

    private void finishBattle() {
        super.printBattleResults();
    }

    private void playBattle() {
        super.printPreCycleInfo(actQueue);
        cycleActQueue();
        //TODO: when a drone has no orders, stop cycle to give it new orders.
        //TODO: make a finished Act perform a relevant effect.
        //TODO: add drone actor/target selection and auto-select
        if (!actQueue.isEmpty()) {
            playBattle();
        }
    }

    private void cycleActQueue() {
        Iterator<Act> acts = actQueue.iterator();
        while (acts.hasNext()) {
            Act act = acts.next();
            if(act.isFinished()) {
                super.finishAction(act);
                acts.remove();
                return;
            }
        }
        super.printProgressBar();
        cycleActQueue();
    }




    private void initBattle() {
        GridUtil.addTerrain(grid);
        GridUtil.cropLocales(grid);

        pl.addDrone(new Drone(Chassis.MEDIUM, Turret.ROCKET));
        ai.addDrone(new Drone(Chassis.MEDIUM, Turret.ROCKET));
        Drone plDrone = pl.getDrone(0);
        Drone aiDrone = ai.getDrone(0);

        plDrone.setDesignation(pl);
        aiDrone.setDesignation(ai);

        plDrone.getLocation().setCoord(1,2);
        aiDrone.getLocation().setCoord(3,2);
        plDrone.setTarget(aiDrone);



        actQueue.add(new Act(2, Act.LOCK_TARGET, plDrone, aiDrone));
        actQueue.add(new Act(8, Act.TURN_LEFT, aiDrone));
        actQueue.add(new Act(8, Act.TURN_LEFT, aiDrone));
        actQueue.add(new Act(12, Act.TURN_LEFT, aiDrone));

        // TODO: figure out how to make coordinates have one source of truth, because this is confusing:
        grid.getHex(plDrone.getLocation()).setDrone(plDrone);
        grid.getHex(aiDrone.getLocation()).setDrone(aiDrone);

//        GridUtil.printGrid(grid, true);
//        super.getDroneInspection(plDrone);
//        super.getDroneInspection(aiDrone);
    }

}
