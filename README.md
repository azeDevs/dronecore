![img_title.png](https://bitbucket.org/repo/MkKop4/images/3142116357-img_title.png)

**A hexagonal strategy game.**

## Resources ##
* [Game Doc](https://docs.google.com/document/d/1fcDjln90MUN1cOflVPK-MDCzfGdU2oaYt4i989tZewU/edit)
* [Drone Core Pinterest](https://www.pinterest.com/nataliebtee/drone-core/)

## Links ##
* [Phobos Rising version](http://dc.phobosrising.co/)
* [Red Blob Hexagons Blog](http://www.redblobgames.com/grids/hexagons/)

![img_footer.png](https://bitbucket.org/repo/MkKop4/images/1363262042-img_footer.png)